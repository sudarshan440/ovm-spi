class spi_master_monitor extends ovm_monitor;
spi_master_sequence_item seq_item;
virtual spi_master_intf intf;
Wrapper wrapper;
ovm_object dummy;
byte data_bytes[$];
bit ackreceived;
ovm_analysis_port #(spi_master_sequence_item) mon2sbwrite;
ovm_analysis_port #(spi_master_sequence_item) mon2sbread;


//registering
`ovm_component_utils(spi_master_monitor)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 

if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
mon2sbwrite = new("mon2sbwrite",this);
mon2sbread = new("mon2sbread",this);

end
else
ovm_report_info(get_type_name(),"dummy is really dummy");

endfunction : build

bit start = 0;
bit rdwr;
integer writestate =0,readstate =0;
virtual task run();
forever
begin
writedatatransfer();
readdatareceived();
end
endtask
bit [7:0] temp,temp1; 
task writedatatransfer ();
 wait(intf.ss == 1'b0)
			   seq_item = spi_master_sequence_item::type_id::create("seq_item");

			    $display("start condition detected in monitor at writedatatransfer");
	 for(int i =0;i<=8;i++) 
				begin
			    @(negedge intf.sck)	
      
				temp[i] = intf.mosi;  
				$display("mosi at monitor is %d",intf.mosi);
				
				end
				seq_item.data = temp;
              $display("data at monitor is %d",seq_item.data);
				mon2sbwrite.write(seq_item); 
				$display("exiting writedatatransfer state");

 
endtask : writedatatransfer
 
task readdatareceived ();
wait (intf.dir ==  1)
				seq_item = spi_master_sequence_item::type_id::create("seq_item");
				
                for(int i =0;i<= 7;i++) 
				begin
			    @(posedge intf.sck)	
      
				temp1[i] = intf.miso;  
				$display("miso at monitor is %d",intf.miso);
				$display("data at monitor is %d",temp1);
				end
				seq_item.data = temp1;
				 
				mon2sbread.write(seq_item); 


endtask : readdatareceived
 endclass : spi_master_monitor