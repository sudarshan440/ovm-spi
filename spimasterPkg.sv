package spimasterPkg;
   //`include "ovm_pkg.sv"
  // import ovm_pkg::*;
   //`include "ovm_pkg.sv"
   //`include "ovm_macros.svh"
  `include "ovm.svh"
  `include "spi_master_sequence_item.sv"
  `include "spi_master_sequence.sv"
  `include "spi_master_sequencer.sv"
  `include "Wrapper.sv"
  `include "spi_master_driver.sv"
  `include "spi_master_monitor.sv "
   
  `include "spi_master_agent.sv"
  `include "scoreboard.sv"
  `include "spi_master_envirnoment.sv"
endpackage : spimasterPkg
