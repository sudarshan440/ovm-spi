class spi_master_testcase extends ovm_test;
spi_master_sequence seq;
spi_master_sequencer sequencer;
spi_master_environment env;
//registering
`ovm_component_utils(spi_master_testcase)

//constructor
function new(string name = " ", ovm_component parent= null);
super.new(name,parent);

endfunction : new

function void build();
  super.build();
  env = spi_master_environment::type_id::create("env",this);
endfunction : build

task run();
$cast(sequencer,env.agent.sequencer);
sequencer.count = 0;
  seq = spi_master_sequence::type_id::create("seq"); 
seq.start(sequencer,null);
endtask 

endclass : spi_master_testcase