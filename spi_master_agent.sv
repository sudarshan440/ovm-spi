class spi_master_agent extends ovm_agent;
spi_master_monitor   monitor;
 
spi_master_driver driver;
spi_master_sequencer sequencer;

ovm_analysis_port #(spi_master_sequence_item) agenttb;
ovm_analysis_port #(spi_master_sequence_item) agentdut;

//registering
`ovm_component_utils(spi_master_agent)

//constructor
function new (string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

function void build();
super.build();
agenttb = new("agenttb",this);
agentdut = new("agentdut",this);

sequencer = spi_master_sequencer::type_id::create("sequencer",this);
driver = spi_master_driver::type_id::create("driver",this);
monitor = spi_master_monitor::type_id::create("monitor",this);
 
endfunction : build

function void connect();
 driver.seq_item_port.connect(sequencer.seq_item_export);
monitor.mon2sbwrite.connect(agenttb);
monitor.mon2sbread.connect(agentdut);

endfunction : connect


endclass : spi_master_agent