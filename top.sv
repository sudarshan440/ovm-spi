module top;
  import spimasterPkg::*;
  import spimasterTestPkg::*;
spi_master_intf intf();

initial
begin
intf.spi_clk = 0;
end

always #10 intf.spi_clk = ~intf.spi_clk;

spi_slave dut (.clk(intf.spi_clk),
                 .rst(intf.spi_rst),
                // .receive(intf.receive),
                 .miso(intf.miso),
                 .mosi(intf.mosi),
				 .sck(intf.sck),
				 .dir(intf.dir),
				 .ss(intf.ss)
);


initial
begin
Wrapper wrapper = new("Wrapper");
wrapper.setVintf(intf);

set_config_object("*","configuration",wrapper,0);
run_test("spi_master_testcase");
end


endmodule : top