class spi_master_sequencer extends ovm_sequencer#(spi_master_sequence_item);

//registering
`ovm_component_utils(spi_master_sequencer)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

endclass : spi_master_sequencer