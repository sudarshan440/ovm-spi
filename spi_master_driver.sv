class spi_master_driver extends ovm_driver#(spi_master_sequence_item);
spi_master_sequence_item seq_item;
virtual spi_master_intf intf;
Wrapper wrapper;
ovm_object dummy;
bit [7:0] data; 
bit start;
//registering
`ovm_component_utils(spi_master_driver)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

//run phase
virtual task run ();
reset_dut();
forever 
begin
 
seq_item_port.get_next_item(seq_item);
@(posedge intf.spi_clk )
  data = seq_item.data;
  
  $display("data received at driver  is %d",seq_item.data);
 slave_write(seq_item); 
 slave_read(seq_item); 
seq_item_port.item_done;
 
end //forever loop
endtask : run


task reset_dut();
       intf.spi_rst = 1;
	   intf.ss = 1;
	   intf.mosi = 0;
	   intf.sck = 0;
	   intf.dir = 0;
	   
       #50ns ;
       intf.spi_rst = 0;
       
 endtask
 
task slave_write(spi_master_sequence_item seq_item);
  
  
  #50ns
  intf.ss = 1'b0;
  
 fork
   ClockGen(); //100khz 
  // datatransfer(addr,data);
  datatransfer (seq_item);
      join_any
 intf.dir = 1;
 $display("out of fork loop");
  
 intf. ss = 1; 
 start = 0;
   
 #500ns;
  
endtask : slave_write

task ClockGen();
  forever begin
  //    #0.01ms intf.sck = ~intf.sck;
         #200ns   intf.sck = ~intf.sck;
		            
  end
endtask : ClockGen

 
task datatransfer(spi_master_sequence_item  seq_item);
  
  start = 1;
    if( start ) 
	begin
	 for(int i = 0; i<8;i++)
	  begin
	    @(posedge intf.sck)
	       intf.mosi = seq_item.data[i];
		  // $display("at driver sending write address %d",seq_item.addr[i]);
		   $display("value i is %d",i);
		   $display("at driver sending write address %d",intf.mosi); 
		   end // for loop
        start  = 0;
    end  

 endtask  

 task slave_read(spi_master_sequence_item seq_item);
   
   
  start = 1'b1;
 fork
   ClockGen(); //100khz 
   //datareceive(addr,data);
   datareceive(seq_item);
 join_any
 
   
   start = 1'b0;
   intf.dir = 0;

  
endtask : slave_read 

bit [7:0] slavedata;

task datareceive(spi_master_sequence_item seq_item);

if( start ) 
	begin
	  intf.dir = 1;
	 for(int i = 0; i<8;i++)
	  begin
	    @(posedge intf.sck)
	       slavedata[i] = intf.miso ;
		  // $display("at driver sending write address %d",seq_item.addr[i]);
		   $display("value i is %d",i);
		   $display("at driver sending read address %d",intf.miso); 
		   end // for loop
  
    end  
 
 endtask : datareceive
 

endclass : spi_master_driver 