module spi_slave(clk,rst,sck,mosi,miso,ss,dir);

input clk,rst;
input mosi;
input sck;
input ss;
input dir;

output  reg miso;



wire [7:0] SPCR= 8'b01110001;
reg [7:0] SPSR = 8'b00000000;
reg [7:0] SPDR = 8'b00000000;

integer counter_4_SPSR=0;
integer misocounter=0,mosicounter=0;
reg misorising=0;
reg misofalling=0;
  
reg [7:0] mosi_temp = 8'b00000000; 
reg [7:0] miso_temp = 8'b00000000; 
reg state   = 0;
  
//assign SPDR = (dir== 1)?mosi_temp: miso_temp;  

//  assign SPSR[7] = (activemosi== 1)?1:0;
  /*
  always @(negedge ss,dir)
  begin
    if(dir == 1)
      SPSR[7] <= 1;
    else
      SPSR[7] <= 0;
  end
 */
  
 
///assign dir = (mosicounter == 7)?1'b1:1'b0;  
//mosi rising
always @(negedge sck)
begin
if (ss == 0  )
  begin
        
  
	  //if (SPSR[7]==1)
    //	 begin
  	     		
      //     if ((SPCR[5] == 1) && (SPCR[3] == SPCR[2])) //lsb
	   //      begin
               if (mosicounter >= 7)
                 begin
                   mosicounter <= 0;
               //    dir <= 1'b1;
				   $display("slave received data is %d",mosi_temp);
                 end
                else 
                 begin
                    //  SPDR <= {mosi,SPDR[7:1]};
                      mosi_temp <= {mosi,mosi_temp[7:1]};
                      mosicounter <= mosicounter + 1;
                      $display("im hereeeeeeeee");   
    
                 end
	         end // if loop
/*
           else if ((SPCR[5] == 0) && (SPCR[3] == SPCR[2])) //msb
            	begin
	              if (mosicounter == 7)
                  begin
                     mosicounter <= 0;
                     dir <= 1'b0;
                  end
                  else 
                  begin
                      $display("im hereeeeeeeee");   
                      SPDR <= {SPDR[6:0],mosi};
                      mosicounter <= mosicounter + 1;
                  end      
      
		
	           end // elseif loop	
	*/
      //       else
        //         begin
          //        $display("sme thing is wrng");  
               
            //     end 
  // end 
 // end    

end //always


//mosi falling
/*
always @(negedge sck)
begin
if (ss == 0) 
begin    
  if (SPSR[7]==1)
   begin
     if ((SPCR[5] == 1) && (SPCR[3] != SPCR[2])) //lsb
	begin
      if (mosicounter > 7)
        begin
          mosicounter <= 0;
          dir <= 1'b0;
        end
      else 
         begin
           //$display("im hereeeeeeeee");   
           SPDR <= {mosi,SPDR[7:1]};
           //mosi_temp <= {mosi,mosi_temp[7:1]};
           mosicounter <= mosicounter + 1;
         end      
           
		
	end // if loop

     else if ((SPCR[5] == 0) && (SPCR[3] == SPCR[2])) //msb
	begin
	        
      if (mosicounter > 7)
        begin
          mosicounter <= 0;
          dir <= 1'b0;
        end
      else 
         begin
           SPDR <= {SPDR[6:0],mosi};
           mosicounter <= mosicounter + 1;
         end 
		
	end // elseif loop	
	
    else
         begin
           $display("sme thing wrng");     
         end    
end
end //while
end //always


*/
  
//miso rising
 always @(negedge sck)
begin

case (state) 

0: 
   begin
     if (dir == 1)
	   begin
	    state <= 1;
		SPDR <= mosi_temp;
		$display("SPDR value is %d",SPDR);
		end
   end

1:
    begin
            if (misocounter > 7)
             begin
		       misocounter <= 0;
             //  dir <= 0;
			   state <= 0;
		 end // miso counter
	   else 
  	     begin
                miso  <= SPDR[0];
           SPDR <= {1'b0,SPDR[7:1]};
		misocounter <= misocounter + 1;
           
	     end //else			
      $display("im here ");	
	end // if loop

endcase	

    end
	
 
   
	
     // if ((SPCR[5] == 1) && (SPCR[3] == SPCR[2])) //lsb
	//begin
	    
 //   else
   //      begin
     //     misorising <= 1'b0;  
               
       //  end    

    //end //dir
//end //always

/*
  always @(negedge sck)
begin
  if (dir == 0)
    begin
      if ((SPCR[5] == 0) && (SPCR[3] == SPCR[2])) //msb
	begin
      
	   if (misocounter > 7)
             begin
	        	misocounter <= 0;
               dir <= 1;
             end
  	   else 
  	     begin
                misorising <= SPDR[7];
         		SPDR <= {SPDR[6:0],1'b0};
		        misocounter <= misocounter + 1;
	     end //else			
			
	end // elseif loop	
    end //dir		
end //always
  

//miso falling
  always @(posedge sck)
begin
  if (dir == 0)
    begin
      if ((SPCR[5] == 1) && (SPCR[3] != SPCR[2])) //lsb
	begin
      
	   if (misocounter > 7)
             begin
     		misocounter <= 0;
            dir <= 1;  
           end // miso counter
	   else 
  	     begin
                misofalling <= SPDR[0];
           SPDR <= {1'b0,SPDR[7:1]};
		misocounter <= misocounter + 1;
	     end //else			
			
	end // if loop

    
    else
         begin
          misorising <= 1'b0;  
               
         end    

    end //dir
end //always

  always @(posedge sck)
begin
  if (dir == 0)
    begin
      if ((SPCR[5] == 0) && (SPCR[3] == SPCR[2])) //msb
	begin
      
	   if (misocounter > 7)
             begin
		misocounter <= 0;
               dir <= 1;
		
	     end // miso counter
	   else 
  	     begin
                misorising <= SPDR[7];
		SPDR <= {SPDR[6:0],1'b0};
		misocounter <= misocounter + 1;
	     end //else			
			
	end // elseif loop	
    end//dir  
end //always
	  
  assign   miso = ((SPCR[5] == 1) &&(SPCR[3] == SPCR[2]))? misorising:misofalling;
 */   

endmodule