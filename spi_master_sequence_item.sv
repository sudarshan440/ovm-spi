class spi_master_sequence_item extends ovm_sequence_item;
// variable declaration 
 
rand bit [7:0] data;
// registering with factory
`ovm_object_utils(spi_master_sequence_item)

//constructor
function new(string name = " " );
super.new(name);
endfunction: new

//declaration of constraints

endclass : spi_master_sequence_item