class scoreboard extends ovm_component;
`ovm_object_utils(scoreboard)
spi_master_sequence_item datafromtb;
spi_master_sequence_item datafromdut;
ovm_analysis_export #(spi_master_sequence_item) sb_tb;
ovm_analysis_export #(spi_master_sequence_item) sb_dut;
tlm_analysis_fifo #(spi_master_sequence_item) gettb;
tlm_analysis_fifo #(spi_master_sequence_item) getdut;

function new (string name = "", ovm_component parent = null);
super.new(name,parent);
datafromdut = new();
datafromtb = new();
endfunction : new

function void build();
super.build();
 sb_tb= new("sb_tb",this);
 sb_dut= new("sb_dut",this);
 gettb= new("gettb",this);
 getdut= new("getdut",this);
endfunction : build

function void connect();
sb_tb.connect(gettb.analysis_export); //dut
sb_dut.connect(getdut.analysis_export); //dut
endfunction : connect

task run();
  forever
begin
gettb.get(datafromtb);
getdut.get(datafromdut);
$display("in scoreboard");
 compare(); 
end
endtask: run

function void compare();
 if(datafromtb.data == datafromdut.data)
  ovm_report_info(get_type_name(),"packet matches",OVM_LOG);
 else
  ovm_report_info(get_type_name(),"packet mismatches",OVM_LOG);
 
endfunction : compare
endclass : scoreboard