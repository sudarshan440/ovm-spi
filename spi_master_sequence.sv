class spi_master_sequence extends ovm_sequence#(spi_master_sequence_item);
spi_master_sequence_item seq_item;

//registering 
`ovm_object_utils(spi_master_sequence)

//constructor

function new(string name = " ");
super.new(name);
endfunction 

// body

virtual task body();
seq_item = spi_master_sequence_item::type_id::create("seq_item");
wait_for_grant( );
seq_item.randomize() with {data == 8'b01010101;};
send_request(seq_item);
wait_for_item_done();
 endtask : body
endclass : spi_master_sequence